<?php

declare(strict_types=1);

namespace App\JsonRpc;

interface CalculatorServiceInterface
{
    public function add(int $a, int $b):int;

//    public function sum(MathValue $v1, MathValue $v2): MathValue;
}
