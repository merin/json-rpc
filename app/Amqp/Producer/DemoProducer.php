<?php

declare(strict_types=1);

namespace App\Amqp\Producer;

//use http\Client\Curl\User;
use Hyperf\Amqp\Annotation\Producer;
use Hyperf\Amqp\Message\ProducerMessage;

/**
 * @Producer(exchange="hyperf", routingKey="hyperf")
 */
#[Producer(exchange: 'hyperf', routingKey: 'hyperf')]
class DemoProducer extends ProducerMessage
{
//    public function __construct($data)
//    {
//        $this->payload = $data;
//    }
    public function __construct($id)
    {
        // 设置不同 pool
        $this->poolName = 'default';

//        $user = User::where('id', $id)->first();
        $this->payload = [
            'id' => $id,
            'data' =>[
                'name' => 'hello world'
            ]
        ];
    }
}
