<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use App\Amqp\Producer\DemoProducer;
use App\JsonRpc\CalculatorServiceInterface;
use App\JsonRpc\MathValue;
use Hyperf\Amqp\Producer;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\Utils\ApplicationContext;

/**
 * @AutoController()
 * Class IndexController
 * @package App\Controller
 */
class IndexController extends AbstractController
{
    public function index()
    {
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        return [
            'method' => $method,
            'message' => "Hello {$user}.",
        ];
    }

    public function add()
    {
        $client = ApplicationContext::getContainer()->get(CalculatorServiceInterface::class);

//        /** @var  $result */
//        $result = $client->add(10,20);
//        var_dump($result);
//        var_dump($result->value);
        return $client->add(10,20);
    }

    public function sum()
    {
        $client = ApplicationContext::getContainer()->get(CalculatorServiceInterface::class);

        /** @var MathValue $result */
        $result = $client->sum(new MathValue(1), new MathValue(2));

        var_dump($result->value);
    }
    public function test()
    {
        $message = new DemoProducer(1);
        $producer = ApplicationContext::getContainer()->get(Producer::class);
        $result = $producer->produce($message);
    }

}
